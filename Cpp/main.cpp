#include "svm.h"
#include <string>
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

#define Malloc(type,n) (type *)malloc((n)*sizeof(type))

class morph{
    public:
        int diamond;
        int rectangle;
        string name;
        morph(int _d, int _r) {
            diamond = _d;
            rectangle = _r;
        }
        morph(string _n) {
            name = _n;
        }
        morph(){}
};
class Model{
    static void silent(const char *) {}; // silence libsvm training debug output
    static svm_node *make_node(const Mat &A, int r=0) { // for a single row opencv feature
        const float *dataPtr = A.ptr<float>(r); // Get data from OpenCV Mat
        svm_node *x_space = Malloc(svm_node, A.cols+1); // one more for the terminator
        //cout << "svm_node" << endl;
        for (int c=0; c<A.cols; c++) {
            x_space[c].index = c+1;  // Index starts from 1; Pre-computed kernel starts from 0
            //cout << "[" << c+1 << ",";
            x_space[c].value = dataPtr[c];
            //cout << dataPtr[c] << "] ";
        }
        cout << endl;
        x_space[A.cols].index = -1;  // End of sequence
        return x_space;
    }
    svm_model *model;
    svm_problem prob;
    public:

    Model(const String &filename) {
        model = svm_load_model(filename.c_str());
        CV_Assert(model != 0);
        prob.l=0;
    }

    Model(const Mat &A, const Mat &L) {
        svm_set_print_string_function(silent); // comment to see the debug output
        svm_parameter param = {0};
        param.svm_type = C_SVC;
        param.kernel_type = LINEAR;
        param.cache_size = 100;
        param.C = 1;
        param.eps = 1e-3;
        param.probability = 1;

        prob.l = A.rows;
        prob.y = Malloc(double, prob.l);
        prob.x = Malloc(svm_node *, prob.l);
        for (int r=0; r<prob.l; r++) {
            prob.x[r] = make_node(A, r);
            prob.y[r] = L.at<int>(r, 0);
        }
        model = svm_train(&prob, &param);
    }

    ~Model() {
        // fixme: if i ever *use* an svm_problem (it's ok with a model loaded from file),
        //   i can't release it after training, but have to keep it around
        //   for the lifetime of the model (s.a. svm.h:69), why ?
        if (prob.l) { // no need to do anything, if model was loaded from file
            for (int r=0; r<prob.l; r++) free(prob.x[r]);
            free(prob.x);
            free(prob.y);
        }
        svm_free_and_destroy_model(&model);
    }

    void probability(const Mat &query, Mat &result) {
        svm_node *x = make_node(query);

        //cout << model->nr_class << endl;

        double prob_est[model->nr_class];
        double prediction = svm_predict_probability(model, x, prob_est);

        free(x);

//        cout << "prediction = " << prediction << endl
//             << " [0] " << prob_est[0] << endl
//             << " [1] " << prob_est[1] << endl
//             << endl;

    }

    double predict(const Mat &query) { // a row sample
        svm_node *x = make_node(query);
        double prediction = svm_predict(model, x);
        free(x);
        return prediction;
    }

    bool save(const String &filename) {
        svm_save_model(filename.c_str(), model);
    }
};

Mat Descriptor(string cropName);
void buildMatDescriptor(vector<morph> finalTable);
void CropImage(vector<string> badImages);
//////////////////////////////////////////////////////////////////////////////////////////////////////
int main() {
    std::vector<std::string> badImages = {
            "9ac38cd713c248058faf.png",
            "iJA4JZ4AX7GZ001577_1.jpg",
            "i2HGFC2F76GH555141_1.jpg",
            "i19XFB2F82FE236436_1.jpg",
            "i3GCPCREC2FG462822_1.jpg",
            "i2HGFG4A59FH700841_1.jpg",
            "i1C4BJWEG0DL600746_1.jpg",
            "1e8a7c77c695475dab00.jpg",
            "ec41d4a0cbb9438ca7a0.jpg",
            "a4b7444069de462caeae.jpg",
            "10ff279141824736a350.jpg",
            "i19UYA42661A011588_1.jpg",
            "iJA4AD3A35GZ057152_1.jpg",
            "iWAUFGAFCXEN161251_1.jpg",
            "iWAUE8HFF9G1096687_1.jpg",
            "i1FADP3K2XFL329248_1.jpg",
            "i1FBAX2CM3HKA90484_1.jpg",
            "i2T3BFREV3FW402020_1.jpg",
            "i2HGFC2F77GH523718_1.jpg",
            "i5FNYF5H85GB020975_1.jpg",
            "iJA32U2FU9GU002057_1.jpg",
            "i1FMJU1HT6HEA08711_1.jpg",
            "i19UDE2F88GA012060_6.jpg",
            "i19UDE2F88GA012060_1.jpg",
            "i5XXGT4L18HG129700_1.jpg"
    };

    CropImage(badImages);
//        buildMatDescriptor(finalTable);
//        vector<morph> sample;
//        buildMatDescriptor(sample);

    return 0;
}
void CropImage(vector<string> badImages) {
    ///csv with crop image information
    ofstream myfile;
    myfile.open ("imageList.csv");
    for (int ixImg = 0; ixImg < sizeof(badImages); ++ixImg) {
    //for (int ixImg = 0; ixImg < 3; ++ixImg) {
        ///final table
        //vector<morph> finalTable;
        ///file name extension
        string imageName = badImages[ixImg];
        string extension = imageName.substr(imageName.length()-3,3);
        ///read image color
        Mat imgColor = imread("Original/" + imageName,CV_LOAD_IMAGE_COLOR);                                                ///param
        ///convert to gray scale
        Mat imgGrey;
        cv::cvtColor(imgColor, imgGrey, cv::COLOR_BGR2GRAY);
        ///reduce noise with a 3x3 kernel
        Mat detected_edges;
        blur(imgGrey, detected_edges, Size(3,3));
        ///canny edge detector
        int lowThd = 50;                                                                                    ///param
        int ratio = 3;                                                                                      ///param
        int kernel_size = 3;                                                                                ///param
        Canny(detected_edges,detected_edges,lowThd,lowThd*ratio, kernel_size);
        ///morphology operators
        vector<morph> structure = {morph(2,1), morph(1,3)};                                                 ///param
        for (int ixStruct = 0; ixStruct < structure.size(); ++ixStruct) {
            ///close
            Mat imgClose;
            int morph_size_close = structure[ixStruct].diamond;
            ///int morph_size_close = 3;
            Mat element_close = getStructuringElement(MORPH_ELLIPSE,
                                                      Size( 5*morph_size_close+1 , 5*morph_size_close+1 ),
                                                      Point( morph_size_close , morph_size_close ) );       ///param
            morphologyEx(detected_edges,imgClose,MORPH_CLOSE,element_close);
            ///erode
            Mat imgErode;
            int morph_size_erode = structure[ixStruct].rectangle;
            Mat element_erode = getStructuringElement(MORPH_RECT,
                                                      Size( 5*morph_size_erode+1, morph_size_erode+1 ),
                                                      Point( morph_size_erode, morph_size_erode ) );        ///param
            morphologyEx(imgClose,imgErode,MORPH_ERODE,element_erode);
            ///labelling
            Mat labels; Mat stats; Mat centroids;
            connectedComponentsWithStats(imgErode,labels,stats,centroids,8,CV_16U);                         ///param
            ///limits
            float maxHeight = labels.rows * 0.7;                                                            ///param
            float maxWidth = labels.cols * 0.7;                                                             ///param
            float minThdArea = 0.1;                                                                         ///param
            float maxThdArea = 16;                                                                          ///param
            int totalArea = labels.cols * labels.rows;
            ///iterator over all the possible images
            for (int ixCrop = 0; ixCrop < stats.rows; ++ixCrop) {
                ///extract information from stats
                int x = stats.at<int>(Point(0,ixCrop));
                int y = stats.at<int>(Point(1,ixCrop));
                int w = stats.at<int>(Point(2,ixCrop));
                int h = stats.at<int>(Point(3,ixCrop));
                int a = stats.at<int>(Point(4,ixCrop));
                float p = ((float)a/(float)totalArea)*100;
                ///big condition
                if(p > minThdArea && p < maxThdArea &&
                   w > maxThdArea && w < maxWidth &&
                   h > maxThdArea && h < maxHeight
                        )
                {
                    ///crop
                    Mat ROI(imgGrey, Rect(x,y,w,h));
                    ///copy
                    Mat crop;
                    ROI.copyTo(crop);
                    ///create name and write file
                    string cropName =
                            to_string(100+ixImg) +
                            to_string(morph_size_close) +
                            to_string(morph_size_erode) +
                            to_string(ixCrop) + "." + extension;
                    ///create file
                    myfile << cropName << "," << to_string(0) << "\n";
                    ///store in the vector
                    //finalTable.push_back(morph(cropName));
                    imwrite("Crop/" + cropName, crop);
                }///end if
            }///end for stats
            ///release memory
//            imgClose.release();
//            imgErode.release();
//            labels.release();
//            stats.release();
//            centroids.release();
        }///end for structure

        ///release memory
//        imgColor.release();
//        imgGrey.release();
//        detected_edges.release();
        ///
    }
    myfile.close();
    //return finalTable;
}
void buildMatDescriptor(vector<morph> finalTable){
    ///read crop report file
//    ofstream myfile;
//    myfile.open ("decriptorFile.txt");
//    for (int k = 0; k < finalTable.size(); ++k)
//    {
//        Mat hist = Descriptor(finalTable[k].name);
//        myfile << "0\t";
//        for (int y = 0; y < hist.rows; ++y)
//        {
//            myfile << to_string(y) + ":" + to_string(hist.at<float>(y,0)) + " ";
//        }
//        myfile << "\n";d
//    }
//    myfile.close();
//    ///
//    Mat MatDescriptors;
//    for (int k = 0; k < finalTable.size(); ++k)
//    {
//        Mat hist = Descriptor(finalTable[k].name);
//        MatDescriptors.push_back(hist.t());
//    }
    ///
    Mat MatDescriptors,MatLabels;
    string line;
    ifstream myfile ("imageListGT.txt");
    if (myfile.is_open())
    {
        while (getline(myfile,line))
        {
            string delimiter = ",";
            string image = line.substr(0, line.find(delimiter));
            string label = line.substr(line.find(delimiter)+1, line.length());

            Mat hist = Descriptor(image);
            MatDescriptors.push_back(hist.t());
            Mat row = (Mat_<int>(1,1) << atoi( label.c_str() ));
            MatLabels.push_back(row);
        }
        myfile.close();
    }
    ///svm
    Model model(MatDescriptors,MatLabels); // from opencv data
    model.save("testModel.txt");
    int count = 0;
    for (int ixDesc = 0 ; ixDesc < MatDescriptors.rows ; ixDesc++ ) {
        Mat result;
//        cout << "test = " << endl << " " << MatDescriptors.row(r) << endl;
        //model.probability(MatDescriptors.row(r), result); // prediction, probA, probB
        //cerr << "prob " << r+1 << " " << result.t() << endl;
        int prediction = model.predict(MatDescriptors.row(ixDesc));
        //cerr << "pred " << r+1 << " " << prediction << endl;
        int pixelValue = MatLabels.at<int>(ixDesc,0);

        if (prediction == pixelValue)
            count++;
    }
    cout << (count/MatDescriptors.rows)*100 << endl;
}
Mat Descriptor(string cropName){
    ///read image
    Mat imgCrop = imread(cropName,CV_LOAD_IMAGE_GRAYSCALE);                                   ///param
    int h = imgCrop.rows;
    int w = imgCrop.cols;
    ///parameters for the filter
    int depth = 8;
    int matDepth = 3;
    Point anchor = Point( -1, -1 );
    double delta = 0;
    int thdLTP = 5;
    ///cordinates for the masks
    int coord[depth][2] = {{1,2},{0,2},{0,1},{0,0},{1,0},{2,0},{2,1},{2,2}};
    ///result
    Mat codePosMat = Mat::zeros(h, w, CV_8U );
    Mat codeNegMat = Mat::zeros(h, w, CV_8U );
    for (int ixCrop = 0; ixCrop < depth; ++ixCrop)
    {
        ///mask
        Mat Mask = Mat::zeros(3, 3, CV_8S );
        Mask.at<uchar>(1,1) = -1;
        Mask.at<uchar>(coord[ixCrop][0],coord[ixCrop][1]) = 1;
        ///filter
        Mat imgFilter = Mat(h, w, CV_16S );
        filter2D(imgCrop, imgFilter, matDepth , Mask, anchor, delta, BORDER_DEFAULT );
        ///vectors of mat for the threshold
        Mat thdMat = Mat::zeros(h, w, CV_8U );
        Mat posMat = Mat::zeros(h, w, CV_8U );
        Mat negMat = Mat::zeros(h, w, CV_8U );
        ///discard values less than in absolute value
        cv::threshold(abs(imgFilter) , thdMat, thdLTP, 1, 0);
        ///keep only positive responses, discarding others
        cv::threshold(imgFilter, posMat, 0, 1, 0);
        posMat = thdMat.mul(posMat);
        ///keep only negative responses
        cv::threshold(imgFilter, negMat, 0, 1, 1);
        negMat = thdMat.mul(negMat);
        ///binary codification
        add(posMat*std::pow(2,ixCrop), codePosMat, codePosMat, Mat(), CV_8U);
        add(negMat*std::pow(2,ixCrop), codeNegMat, codeNegMat, Mat(), CV_8U);
    }
    ///concatenate image
    Mat concatMat = Mat::zeros(h*2, w, CV_8U );
    vconcat(codePosMat, codeNegMat, concatMat);
    ///Establish the number of bins
    int histSize = 256;
    ///Set the ranges ( for B,G,R) )
    float range[] = { 0, 255 } ;
    const float* histRange = { range };
    bool uniform = true;
    bool accumulate = false;
    ///divide images
    int NoDiv = 4;
    int step = (h*2) / NoDiv;
    ///concatenate histogram
    Mat concatHist, normHist;
    vconcat(codePosMat, codeNegMat, concatMat);
    for (int ixHist = 0; ixHist < NoDiv; ++ixHist)
    {
        /// ROI
        Mat ROIpiece(concatMat, Rect(0,ixHist*step,w,step));
        ///copy
        Mat piece; ROIpiece.copyTo(piece);
        ///histogram
        Mat histMat;
        calcHist(&piece, 1, 0, Mat(), histMat, 1, &histSize, &histRange, uniform, accumulate );
        ///concatenation
        concatHist.push_back(histMat);
    }
    //normalize to [1-1000]
    cv::normalize(concatHist, normHist, 1, 1000, NORM_MINMAX, -1, Mat());
    return normHist;
}