inputFile = 'trainFile012.txt';
%%
for i = 0:1000:3000
    gamma = strcat('1.0e-',num2str(i));
    cost = strcat('1.0e+',num2str(i));
    codeTrain = ['svm-train.exe -g ',gamma,' -c ',cost,' -v 4 ',inputFile];
    [status,cmdout] = system(codeTrain);
    lines = regexp(cmdout,'\n','split');
    acc = regexp(lines{end-1},'\d+.\d{4}\%','match','once');
    fprintf('[%d] -> %s\n',i,acc);
end

%%
% 
% for i = 1:5
%     codeTrain = ['svm-train.exe -t 1 -d ',num2str(i),' -r 800 -v 3 ',inputFile];
%     [status,cmdout] = system(codeTrain);
%     lines = regexp(cmdout,'\n','split');
%     acc = regexp(lines{end-1},'\d+.\d{4}\%','match','once');
%     fprintf('[%d] -> %s\n',i,acc);
% end
% -t 1 -d 5 -r 1000 -v 4
% -t 2 -g 0.00001 -c 5000 -b 1