
csvFile = '..\Output\train012.csv';
outputFile = '..\Output\trainFile012.txt';
imgDataBase = '..\images\crop_images_2703\';
%%
M = csvread(csvFile,0,0);
refTrain = cell(size(M,1),1);
bins = 256;
division = 2;
idx = 1:(bins*division);
for i = 1:size(M,1)
    fileName = strcat(int2str(M(i,1)),'-',int2str(M(i,2)),'-',int2str(M(i,3)),'-',int2str(M(i,4)),'.jpg');
    I = imread(strcat(imgDataBase,fileName));
    vidHeight = size(I,1);
    step = vidHeight / division;
    Igray = rgb2gray(I);
%     newI = LDN(Igray,3);
    newI = LTP(Igray,3);
    HH = [];
    for div = 1:step:vidHeight
        crop = newI(div:((div+step)-1),:);
        tempH = hist(crop(:),1:bins);
        HH = cat(2,HH,tempH);
    end
    data = [idx;HH/sum(HH)];
    refTrain{i} = sprintf('%d %s',M(i,5),sprintf(' %d:%0.7f',data));
end

fid = fopen(outputFile, 'wt');
fprintf(fid,'%s\n',refTrain{:});  
fclose(fid);

% fid = fopen('testFile.txt', 'wt');
% fprintf(fid,'%s\n',refTrain{:});  
% fclose(fid);
