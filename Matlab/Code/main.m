clear;

folder = 'bad\'; imageFolder = badImages; badImages = database('bad');
% folder = 'good\'; imageFolder = goodImage; goodImage = database('good');
% 4 - 5
% 3 - 15
fileContent = zeros(0,4);
% [diamond line]
structSize = [4 5; 3 15;];
% iteration      diamond       line
% 1st               4            5
% 2nd               3            15
for params = 1:size(structSize,1)
    for i = 1:size(imageFolder,1)
%         disp(strcat(int2str(diamond),'-',int2str(line)));
        %create folder
        folderName = extractBefore(imageFolder{i},'.jpg');
        %color
        Irgb = imread(strcat(folder,imageFolder{i}));
        %gray
        Igray = double(rgb2gray(Irgb));
        %edges
        Iedge = edge(Igray, "Canny");
        %preserve   
        dmd1 = strel('diamond',structSize(params,1));
        Iclose = imclose(Iedge, dmd1);
        dmd2 = strel('line',structSize(params,2),0);
        Ierode = imerode(Iclose, dmd2);
        %labeling
        Ilabel = bwlabeln(Ierode);
        %count
        [n,L]=hist(Ilabel(:),unique(Ilabel));
        % results
        DB = cell(1,9); 
        %denominador
        sizeI = size(Igray(:),1);
        for ii = 1:size(n,2)
            [rr, cc] = find(Ilabel == L(ii));

            min_rr = min(rr); max_rr = max(rr);
            min_cc = min(cc); max_cc = max(cc);

            DB{ii,1} = max_cc-min_cc+1;                                         %width
            DB{ii,2} = max_rr-min_rr+1;                                         %heigth
            DB{ii,3} = min_rr;                                                  %UL-Y
            DB{ii,4} = max_rr;                                                  %DR-Y          
            DB{ii,5} = min_cc;                                                  %UL-X
            DB{ii,6} = max_cc;                                                  %DR-X          
            DB{ii,7} = DB{ii,1} * DB{ii,2};                                     %area
            DB{ii,8} = (DB{ii,7}/sizeI)*100;                                    %percentage
        end
        thdMin = 0.1;
        thdMax = 15;
        h = size(Igray,1)*0.8;
        w = size(Igray,2)*0.8;
        for jj = 1:size(DB,1)
            DB{jj,9} = 0;
            if (DB{jj,8} > thdMin && DB{jj,8} < thdMax && ...
                DB{jj,1} > thdMax && DB{jj,1} < w && ...
                DB{jj,2} > thdMax && DB{jj,2} < h)  
    %         if (DB{jj,8} > thdMin && DB{jj,8} < thdMax)  
                DB{jj,9} = 1;                                                   %valid
            end
            DB{jj,10} = L(jj);                                                  %label
            %
            if(DB{jj,9} > 0)
                fileName = strcat(int2str(i),'-',int2str(structSize(params,1)),'-',int2str(structSize(params,2)),'-',int2str(DB{jj,10}),'.jpg');
                fileContent(end+1,:) = [i,structSize(params,1),structSize(params,2),DB{jj,10}];
                fileImage = strcat('crop_images_2703\',fileName);
                imwrite(Irgb(DB{jj,3}:DB{jj,4},DB{jj,5}:DB{jj,6},:),fileImage);
            end
        end
    end 
end

csvwrite('train012.csv',fileContent);


    

%%

% fid = fopen('train012.csv', 'w');
% fprintf(fid, '%s\n', fileContent{1,:}) ;
% fclose(fid);

%             disp(max(cell2mat(DB(2:end,8))));
%             disp(mean(cell2mat(DB(2:end,8))));
%             disp(min(cell2mat(DB(2:end,8))));
%             disp(median(cell2mat(DB(2:end,8))));
%             disp(mode(cell2mat(DB(2:end,8))));



% StructList = {'diamond','octagon','square','cube','sphere'};
%     dmddiamond = strel('diamond',1);
%     Iclosediamond = imclose(Iedge, dmddiamond);
%     figure, imshow(Iclosediamond,[]), title('Iclosediamond');
%     
%     dmddisk = strel('disk',1);
%     Iclosedisk = imclose(Iedge, dmddisk);
%     figure, imshow(Iclosedisk,[]), title('Iclosedisk');
%     
%     dmdline = strel('line',5,5);
%     Icloseline = imclose(Iedge, dmdline);
%     figure, imshow(Icloseline,[]), title('Icloseline');
% 
%     dmdoctagon = strel('octagon',3);
%     Icloseoctagon = imclose(Iedge, dmdoctagon);
%     figure, imshow(Icloseoctagon,[]), title('Icloseoctagon');
% 
%     dmdrectangle = strel('rectangle',[10 5]);
%     Icloserectangle = imclose(Iedge, dmdrectangle);
%     figure, imshow(Icloserectangle,[]), title('Icloserectangle');
% 
%     dmdsquare = strel('square',1);
%     Iclosesquare = imclose(Iedge, dmdsquare);
%     figure, imshow(Iclosesquare,[]), title('Iclosesquare');
% 
%     dmdcube = strel('cube',1);
%     Iclosecube = imclose(Iedge, dmdcube);
%     figure, imshow(Iclosecube,[]), title('Iclosecube');
% 
%     dmdsphere = strel('sphere',1);
%     Iclosesphere = imclose(Iedge, dmdsphere);
%     figure, imshow(Iclosesphere,[]), title('Iclosesphere');

%     figure, imshow(Idilate1,[]), title('1');
%     dmd2 = strel('diamond',2);
%     Idilate2 = imdilate(Iedge, dmd2);
%     figure, imshow(Idilate2,[]), title('2');
%     dmd3 = strel('diamond',3);
%     Idilate3 = imdilate(Iedge, dmd3);
%     figure, imshow(Idilate3,[]), title('3');
    
%     Iedge1 = edge(Igray, "Sobel");
%     figure, imshow(Iedge1,[]), title('Sobel');
%     Iedge2 = edge(Igray, "Prewitt");
%     figure, imshow(Iedge2,[]), title('Prewitt');
%     Iedge3 = edge(Igray, "Roberts");
%     figure, imshow(Iedge3,[]), title('Roberts');
%     Iedge4 = edge(Igray, "log");
%     figure, imshow(Iedge4,[]), title('log');
%     Iedge5 = edge(Igray, "zerocross");
%     figure, imshow(Iedge5,[]), title('zerocross');
%     Iedge6 = edge(Igray, "Canny");
%     figure, imshow(Iedge6,[]), title('Canny');
%     Iedge7 = edge(Igray, "approxcanny");
%     figure, imshow(Iedge7,[]), title('approxcanny');

%     subplot(1,3,1), imshow(Igray,[]);
%     subplot(1,3,2), imshow(Iedge,[]);  
%     subplot(1,3,3), imshow(Idilate,[]);

%     Idilate = imdilate(Iedge, dmd);
%     
%     Ilabel = bwlabeln(Idilate);
%     [n,L]=hist(Ilabel(:),unique(Ilabel));
%     thd = round(mean(n(2:end)));
%     ixLabel = L(n>thd);
%     
%     newI = zeros(size(Igray));
%     for ii = 2:size(ixLabel,1)
%         newI = newI + (Ilabel==ixLabel(ii))*ixLabel(ii);
%     end
%     
%     subplot(2,2,1), imshow(Igray,[])
%     subplot(2,2,2), imshow(Iedge,[])
%     subplot(2,2,3), imshow(Ilabel>0,[])
%     subplot(2,2,4), imshow(newI>0,[])

        
%     txt = ocr(Iedge);
    
%     Ilabel2 = bwlabeln(Iedge2); % 1109
%     [n2,L2]=hist(Ilabel2(:),unique(Ilabel2));
%     thd2 = mean(n2(2:end));
%     Iedge3 = Ilabel2>thd2;
%     
%     Ilabel3 = bwlabeln(Iedge3); % 1109
%     [n3,L3]=hist(Ilabel3(:),unique(Ilabel3));
%     thd3 = mean(n3(2:end));
    
%     imshow(Iprewitt,[]);

%     subplot(1,2,1), imshow(Iprewitt,[])
%     subplot(1,2,2), imshow(Iprewitt2,[])
%     subplot(2,2,3), imshow(Iprewitt2,[])
%     subplot(2,2,4), imshow(Iprewitt2,[])
    
%     Ienhanced = Igray+Iprewitt;
%     Iprewitt2 = edge(Ienhanced, "Prewitt");
    
%     Ienhanced = Igray+Iprewitt;
%     Ierode = imerode(Ienhanced, dmd);
%     Iprewitt2 = edge(Ierode, "Prewitt");
    
%     Ienhanced = Igray+Iprewitt;
%     Idilate = imdilate(Ienhanced, dmd);
%     Iprewitt2 = edge(Idilate, "Prewitt");

   
%Iblur = imgaussfilt(Igray,2);
%imshow(Iblur,[]);
%Iedge = edge(Iblur,'Canny');

%Iedge = edge(Igray,'Canny');
%imshow(Iedge,[]);

% [mserRegions, mserConnComp] = detectMSERFeatures(Igray);
% figure
% imshow(Igray)
% hold on
% plot(mserRegions, 'showPixelList', true,'showEllipses',false)
% title('MSER regions')
% hold off
